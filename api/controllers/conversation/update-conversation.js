module.exports = {


  friendlyName: 'UpdateConversation',


  description: 'Update a conversation',


  extendedDescription:
`This updates a conversation`,


  inputs: {

    id: {
      required: true,
      type: 'number',
      description: 'The id of the conversation.',
    },

    lastCheckedAt: {
      type: 'number',
    },

    lastClickedAt: {
      type: 'number',
    },

    lastReadAt: {
      type: 'number',
    },

  },


  exits: {

    success: {
      description: 'The conversation has been successfully updated.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    var valuesToSet = { }
    
    if(inputs.hasOwnProperty('lastCheckedAt')) { valuesToSet.lastCheckedAt = inputs.lastCheckedAt; }
    if(inputs.hasOwnProperty('lastClickedAt')) { valuesToSet.lastClickedAt = inputs.lastClickedAt; }
    if(inputs.hasOwnProperty('lastReadAt')) { valuesToSet.lastReadAt = inputs.lastReadAt; }

    conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests').populate('formerMembers');

    if(!conversation.participants.concat(conversation.guests).concat(conversation.formerMembers).map( (element) => { return element.id } ).includes(this.req.me.id)) {
      throw 'unauthorized';
    }

    if(conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      await participant_conversation.updateOne({ participant: this.req.me.id, conversation: inputs.id })
      .set(valuesToSet);
    }
    else if(conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      await guest_invitation.updateOne({ guest: this.req.me.id, invitation: inputs.id })
      .set(valuesToSet);
    }
    else if(conversation.formerMembers.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      await formerMember_previousConversation.updateOne({ formerMember: this.req.me.id, previousConversation: inputs.id })
      .set(valuesToSet);
    }

    conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests').populate('formerMembers');

    if(conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      var association = await participant_conversation.findOne({ participant: this.req.me.id, conversation: inputs.id });
    }
    else if(conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      var association = await guest_invitation.findOne({ guest: this.req.me.id, invitation: inputs.id });
    }
    else if(conversation.formerMembers.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      var association = await formerMember_previousConversation.findOne({ formerMember: this.req.me.id, previousConversation: inputs.id });
    }
    conversation.lastCheckedAt = association.lastCheckedAt;
    conversation.lastClickedAt = association.lastClickedAt;
    conversation.lastReadAt = association.lastReadAt;

    exits.success(conversation.toJSON());

  }

};
