module.exports = {


  friendlyName: 'LeaveConversation',


  description: 'Leave a conversation',


  extendedDescription:
`This removes the request's author from a conversation and destroy the conversation if there is no more participant`,


  inputs: {

    id: {
      required: true,
      type: 'number',
      description: 'The id of the conversation.',
    },

  },


  exits: {

    success: {
      description: 'The user has been successfully removed from the conversation.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

  },


  fn: async function (inputs, exits) {

    var conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests').populate('formerMembers');

    if(!conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id) && !conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id) && !conversation.formerMembers.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      throw 'unauthorized';
    }

    await Conversation.removeFromCollection(inputs.id, 'guests', this.req.me.id);
    if(conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      await Conversation.removeFromCollection(inputs.id, 'participants', this.req.me.id);
      await Conversation.addToCollection(conversation.id, 'formerMembers').members([this.req.me.id]);
    }
    else if(!conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id) && conversation.formerMembers.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      await Conversation.removeFromCollection(inputs.id, 'formerMembers', this.req.me.id);
    }

    if(conversation.participants.concat(conversation.guests).map( (element) => { return element.id; }).includes(this.req.me.id)) {
      var entry = await Entry.create({
        type: 'leave',
        user: this.req.me.id,
        conversation: inputs.id,
        data: { }
      }).fetch();

      sails.sockets.broadcast(['participants_'+entry.conversation],'entryCreated', entry.toJSON(), this.req);

      sails.sockets.leave(this.req, 'guests_'+conversation.id);
      sails.sockets.leave(this.req, 'participants_'+conversation.id);

      conversation = await Conversation.findOne({
        id: inputs.id
      }).populate('participants').populate('guests');

      if (conversation.participants.length!=0) {
        sails.sockets.broadcast(['participants_'+conversation.id,'guests_'+conversation.id],'conversationEdited', conversation.toJSON());
      }
      else {
        await Conversation.removeFromCollection(conversation.id, 'guests', conversation.guests.map( (guest) => { return guest.id; } ));
        conversation = await Conversation.updateOne({id: conversation.id })
        .set({ status: 'archived' });

        sails.sockets.broadcast('guests_'+conversation.id,'conversationDestroyed', conversation.id);
        sails.sockets.removeRoomMembersFromRooms('guests_'+conversation.id,'guests_'+conversation.id);
      }
    }
    else {
      conversation = await Conversation.findOne({
        id: inputs.id
      }).populate('entries');

      await Entry.update({id: { 'in': conversation.entries.filter( (entry) => { return entry.type=='message' && entry.user==this.req.me.id; }).map( (entry) => { return entry.id; }) } })
      .set({ deleted: true });
    }

    var conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests').populate('formerMembers');

    exits.success({ conversation: conversation.toJSON(), entry: entry!=undefined ? entry.toJSON() : null });

  }

};
