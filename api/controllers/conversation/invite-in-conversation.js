module.exports = {


  friendlyName: 'InviteInConversation',


  description: 'Invite a user in a conversation',


  extendedDescription:
`This invite a user in a conversation`,


  inputs: {

    userId: {
      required: true,
      type: 'number',
      description: 'The id of the user.',
    },

    conversationId: {
      required: true,
      type: 'number',
      description: 'The id of the conversation.',
    },

  },


  exits: {

    success: {
      description: 'The user has successfully been invited in the conversation.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    user = await User.findOne({
      id: inputs.userId
    });

    conversation = await Conversation.findOne({
      id: inputs.conversationId
    }).populate('participants').populate('guests');

    if(!conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      throw 'unauthorized';
    }

    if(conversation.participants.map( (element) => { return element.id; }).includes(user.id) || conversation.guests.map( (element) => { return element.id; }).includes(user.id)) {
      throw 'invalid';
    }

    await Conversation.addToCollection(conversation.id, 'guests', user.id);

    conversation = await Conversation.findOne({
      id: inputs.conversationId
    }).populate('participants').populate('guests').populate('formerMembers');

    sails.sockets.join(user.socketId, 'guests_'+conversation.id);

    sails.sockets.broadcast(['participants_'+conversation.id,'guests_'+conversation.id],'conversationEdited', conversation.toJSON(), this.req);

    var entry = await Entry.create({
      type: 'invited',
      user: user.id,
      conversation: conversation.id,
      data: { inviter: this.req.me.id}
    }).fetch();

    sails.sockets.broadcast(['participants_'+entry.conversation,user.socketId],'entryCreated', entry.toJSON(), this.req);

    exits.success({ conversation: conversation.toJSON(), entry: entry.toJSON() });

  }

};
