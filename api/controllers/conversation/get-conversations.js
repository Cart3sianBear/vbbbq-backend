module.exports = {


  friendlyName: 'GetConversations',


  description: 'Send to client informations about conversations',


  extendedDescription:
`This search for conversations records in the database and send them to client`,


  inputs: {

    ids: {
      required: true,
      type: 'ref',
      description: 'The array of id associated with the requested conversations.',
    },

  },


  exits: {

    success: {
      description: 'The requested conversations were send successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The access to one of the requested conversations is forbidden.',
    },

  },


  fn: async function (inputs, exits) {
    var conversations = [];

    var promise = Promise.resolve();
    inputs.ids.forEach( (id) => {
      promise = promise.then( () => {
        return new Promise( async (resolve) => {

          var conversation = await Conversation.findOne({ id: id }).populate('participants').populate('guests').populate('formerMembers').populate('entries',{
            where: {
              user: this.req.me.id,
              type: {'in': ['join','leave']}
            },
          });

          if(conversation.participants.concat(conversation.guests).concat(conversation.formerMembers).map( (element) => { return element.id } ).includes(this.req.me.id)) {
            if(conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
              var association = await participant_conversation.findOne({ participant: this.req.me.id, conversation: id });
            }
            else if(conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id)) {
              var association = await guest_invitation.findOne({ guest: this.req.me.id, invitation: id });
            }
            else if(conversation.formerMembers.map( (element) => { return element.id; }).includes(this.req.me.id)) {
              var association = await formerMember_previousConversation.findOne({ formerMember: this.req.me.id, previousConversation: id });
            }
            conversation.lastCheckedAt = association.lastCheckedAt;
            conversation.lastClickedAt = association.lastClickedAt;
            conversation.lastReadAt = association.lastReadAt;
          }

          if (conversation.status=='public' || conversation.participants.concat(conversation.guests).concat(conversation.formerMembers).map( (element) => { return element.id } ).includes(this.req.me.id)) {
            var query = { or: [], };
            conversation.entries.forEach( (entry) => {
              if(entry.type=='join' && (query.or.length==0 || Object.keys(query.or[query.or.length-1].id).includes('<='))) {
                query.or.push({ id: { '>=': entry.id } });
              }
              else if(entry.type=='leave' && query.or.length!=0 && !Object.keys(query.or[query.or.length-1].id).includes('<=')) {
                query.or[query.or.length-1].id['<='] = entry.id;
              }
            });
            query.or.push({ type: 'create' });
            query.or.push({ type: 'invited', user: this.req.me.id });
            query.or.push({ type: 'leave', user: this.req.me.id });
            
            query.conversation = conversation.id;

            conversation.entries = [];
            while(conversation.entries.length==0 || !['message','create'].includes(conversation.entries[conversation.entries.length-1].type)) {
              var entry = await Entry.find({
                where: query,
                limit: 1,
                sort: 'id DESC'
              });
              query.id = { '<': entry[0].id };
              conversation.entries.push(entry[0]);
            }

            conversations.push(conversation);
            resolve();
          }
          else {
            exits.unauthorized();
          }
        });
      });
    });

    promise.then( () => {
      conversations.forEach( (conversation) => {
        if(conversation.participants.map( (element) => { return element.id } ).includes(this.req.me.id)) {
          sails.sockets.join(this.req, 'participants_'+conversation.id);
        }
        else if(conversation.guests.map( (element) => { return element.id } ).includes(this.req.me.id)) {
          sails.sockets.join(this.req, 'guests_'+conversation.id);
        }
      });

      exits.success({ conversations: conversations.map( (element) => { return element.toJSON(); }), entries: conversations.map( (element) => { return element.entries.map( (element2) => { return element2.toJSON(); }); }).flat() });
    });

  }

};
