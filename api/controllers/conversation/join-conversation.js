module.exports = {


  friendlyName: 'JoinConversation',


  description: 'Join a conversation',


  extendedDescription:
`This makes the request's author a participant of the conversation`,


  inputs: {

    id: {
      required: true,
      type: 'number',
      description: 'The id of the conversation.',
    },

  },


  exits: {

    success: {
      description: 'The user has successfully joined the conversation.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    var conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests');

    if( conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id) ||  (conversation.status!='public' && !conversation.guests.map( (element) => { return element.id; }).includes(this.req.me.id)) ) {
      throw 'unauthorized';
    }
    else {
      await Conversation.removeFromCollection(inputs.id, 'guests', this.req.me.id);
      await Conversation.addToCollection(inputs.id, 'participants').members([this.req.me.id]);
      await Conversation.removeFromCollection(inputs.id, 'formerMembers', this.req.me.id);
    }

    var entry = await Entry.create({
      type: 'join',
      user: this.req.me.id,
      conversation: conversation.id,
      data: { }
    }).fetch();

    sails.sockets.broadcast(['participants_'+entry.conversation],'entryCreated', entry.toJSON());

    conversation = await Conversation.findOne({
      id: inputs.id
    }).populate('participants').populate('guests').populate('formerMembers');

    sails.sockets.leave(this.req, 'guests_'+inputs.id);

    sails.sockets.broadcast(['participants_'+inputs.id,'guests_'+inputs.id],'conversationEdited', conversation.toJSON());

    sails.sockets.join(this.req, 'participants_'+inputs.id);

    exits.success({ conversation: conversation.toJSON(), entry: entry.toJSON() });

  }

};
