module.exports = {


  friendlyName: 'CreateConversation',


  description: 'Create a new conversation',


  extendedDescription:
`This creates a new conversation record in the database, make it's author a participant and send invitations`,


  inputs: {

    guests: {
      required: true,
      type: 'ref',
      description: 'The array of id associated with the inital guests of this conversation.',
    },

    public: {
      required: true,
      type: 'boolean',
      description: 'Does this conversation should be public ?',
    },

  },


  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

  },


  fn: async function (inputs, exits) {

    if(inputs.guests.includes(this.req.me.id)) {
      throw 'invalid';
    }

    var newConversationRecord = await Conversation.create({
      status: inputs.public ? 'public' : 'private',
    }).fetch();

    await Conversation.addToCollection(newConversationRecord.id, 'participants', this.req.me.id);
    await Conversation.addToCollection(newConversationRecord.id, 'guests', inputs.guests);

    await Entry.create({
      type: 'create',
      user: this.req.me.id,
      conversation: newConversationRecord.id,
      data: { }
    });
    await Entry.create({
      type: 'join',
      user: this.req.me.id,
      conversation: newConversationRecord.id,
      data: { }
    });

    var conversation = await Conversation.findOne({
      id: newConversationRecord.id
    }).populate('participants').populate('guests').populate('formerMembers');

    sails.sockets.join(this.req, 'participants_'+conversation.id);
    conversation.guests.forEach( (element) => {
      sails.sockets.join(element.socketId, 'guests_'+conversation.id);
    });

    sails.sockets.broadcast('guests_'+conversation.id,'conversationCreated', conversation.toJSON());

    conversation.guests.forEach( async (guest) => {
      entry = await Entry.create({
        type: 'invited',
        user: guest.id,
        conversation: conversation.id,
        data: { inviter: this.req.me.id}
      }).fetch();
      sails.sockets.broadcast([guest.socketId],'entryCreated', entry.toJSON(), this.req);
    });

    exits.success(conversation.toJSON());

  }

};
