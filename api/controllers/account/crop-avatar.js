module.exports = async function (req, res) {
  const img = await sails.sharp(process.env.AVATARS_PATH+'/tmp/'+req.me.id);
  img.clone().toFile(process.env.AVATARS_PATH+'/original/'+req.me.id);
  
  const imgExtracted = img.clone().extract({ left: Math.round(req.body.x), top: Math.round(req.body.y), width: Math.round(req.body.w), height: Math.round(req.body.w) });
  imgExtracted.clone().resize({ width: 128, height: 128, }).toFile(process.env.AVATARS_PATH+'/128/'+req.me.id)
  imgExtracted.clone().resize({ width: 64, height: 64, }).toFile(process.env.AVATARS_PATH+'/64/'+req.me.id)
  imgExtracted.clone().resize({ width: 40, height: 40, }).toFile(process.env.AVATARS_PATH+'/40/'+req.me.id)
  imgExtracted.clone().resize({ width: 32, height: 32, }).toFile(process.env.AVATARS_PATH+'/32/'+req.me.id)
  .then( () => {
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/tmp/'+req.me.id+"'");
    return res.ok();
  }).catch( (err) => {
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/tmp/'+req.me.id+"'");
    return res.badRequest('Error during crop & resize');
  });
};
