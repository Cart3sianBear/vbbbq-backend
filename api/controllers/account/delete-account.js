module.exports = {


  friendlyName: 'DeleteAccount',


  description: 'Delete the user account.',


  extendedDescription:
`This delete the user record in the database and all it's entries, and remove him from all conversations, or
(if emailing with Mailgun is enabled) sends an account deletion email.`,


  inputs: {

    emailAddress: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address of the account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol00',
      description: 'The unencrypted password of the account.'
    },

  },


  exits: {

    success: {
      description: 'User account successfully removed or deletion email sent.'
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The specified user is not the current one !',
    },

    deletionForbidden: {
      description: `The deletion is forbidden because of a recent email change.`,
      responseType: 'unauthorized'
    },

    badCombo: {
      description: `The provided email and password combination does not
      match any user in the database.`,
      responseType: 'unauthorized'
    },

  },


  fn: async function (inputs) {

    var userRecord = await User.findOne({
      emailAddress: inputs.emailAddress.toLowerCase(),
    });

    if(!userRecord) {
      throw 'badCombo';
    }

    // If the password doesn't match, then also exit thru "badCombo".
    await sails.helpers.passwords.checkPassword(inputs.password, userRecord.password)
    .intercept('incorrect', 'badCombo');

    // If the account is not confirmed
    if(userRecord.emailStatus!='confirmed') {
      sails.sockets.leaveAll(userRecord.socketId);
      await User.destroyOne({ id: userRecord.id });
    }
    else {
      if(this.req.me.emailAddress!=userRecord.emailAddress) {
        throw 'unauthorized';
      }

      if(userRecord.lastEmailChangeAt!='' && Date.now() < userRecord.lastEmailChangeAt+sails.config.custom.deletionForbiddenDuration) {
        throw 'deletionForbidden';
      }

      if (sails.config.custom.verifyAccountsDeletion) {
        await User.updateOne({id: userRecord.id })
        .set({
          deleteAccountToken: await sails.helpers.strings.random('url-friendly'),
          deleteAccountTokenExpiresAt: Date.now() + sails.config.custom.deleteAccountTokenTTL,
        });

        var userRecord = await User.findOne({
          emailAddress: inputs.emailAddress.toLowerCase(),
        });

        // Send "delete account" email
        await sails.helpers.sendTemplateEmail.with({
          to: userRecord.emailAddress,
          subject: 'Delete your account',
          template: 'email-delete-account',
          templateData: {
            username: userRecord.username,
            token: userRecord.deleteAccountToken
          }
        });
      } else {
        await User.destroyOne({ id: userRecord.id });
        sails.log.info('Skipping new account email verification... (since `verifyEmailAddresses` is disabled)');
      }
    }

  }

};
