module.exports = {


  friendlyName: 'Confirm deletion',


  description:
`Confirm a user deletion.`,


  inputs: {

    token: {
      description: 'The confirmation token from the email.',
      example: '4-32fad81jdaf$329'
    }

  },


  exits: {

    success: {
      description: 'Account deleted forever.'
    },

    invalidOrExpiredToken: {
      responseType: 'expired',
      description: 'The provided token is expired, invalid, or already used up.',
    },

  },


  fn: async function (inputs) {

    // If no token was provided, this is automatically invalid.
    if (!inputs.token) {
      throw 'invalidOrExpiredToken';
    }

    // Get the user with the matching email token.
    var user = await User.findOne({ deleteAccountToken: inputs.token });

    // If no such user exists, or their token is expired, bail.
    if (!user || user.deleteAccountTokenExpiresAt <= Date.now()) {
      throw 'invalidOrExpiredToken';
    }

    await User.destroyOne({ id: user.id });

  }


};
