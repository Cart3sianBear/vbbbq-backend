module.exports = async function (req, res) {
  req.file('avatar').upload({
    // don't allow the total upload size to exceed ~10MB
    maxBytes: 10000000,
    dirname: require('path').resolve(process.env.AVATARS_PATH, 'tmp'),
    saveAs: req.me.id.toString()
  }, function whenDone(err, uploadedFiles) {
    if (err) {
      return res.serverError(err);
    }

    // If no files were uploaded, respond with an error.
    if (uploadedFiles.length === 0){
      return res.badRequest('No file was uploaded');
    }

    sails.sharp(process.env.AVATARS_PATH+'/tmp/'+req.me.id).toBuffer().then( () => {
      return res.ok();
    }).catch( () => {
      sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/tmp/'+req.me.id+"'");
      return res.badRequest('Bad image format');
    });
  });
};
