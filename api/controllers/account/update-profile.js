module.exports = {


  friendlyName: 'Update profile',


  description: 'Update the profile for the logged-in user.',


  inputs: {

    emailAddress: {
      type: 'string',
      isEmail: true,
      description: 'The new email address for the account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      type: 'string',
      maxLength: 200,
      example: 'passwordlol00',
      description: 'The current, unencrypted password.'
    },

    newPassword: {
      type: 'string',
      maxLength: 200,
      example: 'passwordlol00',
      description: 'The new, unencrypted password.'
    },

    username:  {
      type: 'string',
      example: 'MasterOfChaosDu51',
      description: 'The user\'s new username.',
    },

    password:  {
      type: 'string',
      example: 'MasterOfChaosDu51',
      description: 'The user\'s password.',
    },

    placeId: {
      type: 'number',
      description: 'The user\'s new placeId in nominatim\s sense.',
    },

    dashboard: {
      type: 'json',
      description: 'The new state of the user\'s dashboard',
    },

  },


  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

    badPassword: {
      description: `The provided password does not match user in the database.`,
      responseType: 'unauthorized'
      // ^This uses the custom `unauthorized` response located in `api/responses/unauthorized.js`.
      // To customize the generic "unauthorized" response across this entire app, change that file
      // (see api/responses/unauthorized).
      //
      // To customize the response for _only this_ action, replace `responseType` with
      // something else.  For example, you might set `statusCode: 498` and change the
      // implementation below accordingly (see http://sailsjs.com/docs/concepts/controllers).
    },

    emailChangeForbidden: {
      description: `The email change is forbidden because of a recent email change.`,
      responseType: 'unauthorized'
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },

  },


  fn: async function (inputs, exits) {
    // Start building the values to set in the db.
    var valuesToSet = { }
    
    if(inputs.hasOwnProperty('username')) { valuesToSet.username = inputs.username; }
    if(inputs.hasOwnProperty('dashboard')) { valuesToSet.dashboard = inputs.dashboard; }

    const osmQuery = new Promise( (resolve) => {
      if(inputs.hasOwnProperty('placeId')) {
        sails.https.get(process.env.NOMINATIM_URL+"/details.php?place_id="+inputs.placeId+"&format=json&addressdetails=1", (res) => {
          var body = '';
          res.on('data', function (chunk) {
            body = body + chunk;
          });
          res.on('end',function(){
            switch(res.statusCode) {
              case 200:
                resolve(JSON.parse(body));
              break;  
            }
          });
        });
      }
      else {
        resolve(null);
      }
    });

    osmQuery.then( async (place) => {
      if(place!=null) { 
        valuesToSet.placeId = place['place_id'];
        valuesToSet.city = place.address.find( (element) => { return ['village','town','administrative','country'].includes(element.type); }).localname;
        valuesToSet.latitude = place.centroid.coordinates['1'];
        valuesToSet.longitude = place.centroid.coordinates['0'];
      }

      if(inputs.hasOwnProperty('emailAddress')) {
        await sails.helpers.passwords.checkPassword(inputs.password, this.req.me.password)
        .intercept('incorrect', 'badPassword');

        if(this.req.me.lastEmailChangeAt!='' && Date.now() < this.req.me.lastEmailChangeAt+sails.config.custom.emailChangeForbiddenDuration) {
          throw 'emailChangeForbidden';
        }

        var newEmailAddress = inputs.emailAddress;
        if (newEmailAddress !== undefined) {
          newEmailAddress = newEmailAddress.toLowerCase();
        }

        // Determine if this request wants to change the current user's email address,
        // revert her pending email address change, modify her pending email address
        // change, or if the email address won't be affected at all.
        var desiredEmailEffect;// ('change-immediately', 'begin-change', 'cancel-pending-change', 'modify-pending-change', or '')
        if (
          newEmailAddress === undefined ||
          (this.req.me.emailStatus !== 'change-requested' && newEmailAddress === this.req.me.emailAddress) ||
          (this.req.me.emailStatus === 'change-requested' && newEmailAddress === this.req.me.emailChangeCandidate)
        ) {
          desiredEmailEffect = '';
        } else if (this.req.me.emailStatus === 'change-requested' && newEmailAddress === this.req.me.emailAddress) {
          desiredEmailEffect = 'cancel-pending-change';
        } else if (this.req.me.emailStatus === 'change-requested' && newEmailAddress !== this.req.me.emailAddress) {
          desiredEmailEffect = 'modify-pending-change';
        } else if (!sails.config.custom.verifyEmailAddresses || this.req.me.emailStatus === 'unconfirmed') {
          desiredEmailEffect = 'change-immediately';
        } else {
          desiredEmailEffect = 'begin-change';
        }


        // If the email address is changing, make sure it is not already being used.
        if (_.contains(['begin-change', 'change-immediately', 'modify-pending-change'], desiredEmailEffect)) {
          let conflictingUser = await User.findOne({
            or: [
              { emailAddress: newEmailAddress },
              { emailChangeCandidate: newEmailAddress }
            ]
          });
          if (conflictingUser) {
            throw 'emailAlreadyInUse';
          }
        }

        switch (desiredEmailEffect) {
          // Change now
          case 'change-immediately':
            _.extend(valuesToSet, {
              emailAddress: newEmailAddress,
              lastEmailChangeAt: Date.now(),
              emailChangeCandidate: '',
              emailRecoverable: '',
              emailProofToken: '',
              emailProofTokenExpiresAt: 0,
              emailRecoveryToken: '',
              emailRecoveryTokenExpiresAt: 0,
              emailStatus: this.req.me.emailStatus === 'unconfirmed' ? 'unconfirmed' : 'confirmed'
            });
            break;

          // Begin new email change, or modify a pending email change
          case 'begin-change':
          case 'modify-pending-change':
            _.extend(valuesToSet, {
              emailChangeCandidate: newEmailAddress,
              emailRecoverable: this.req.me.emailAddress,
              emailProofToken: await sails.helpers.strings.random('url-friendly'),
              emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
              emailRecoveryToken: await sails.helpers.strings.random('url-friendly'),
              emailRecoveryTokenExpiresAt: Date.now() + sails.config.custom.emailRecoveryTokenTTL,
              emailStatus: 'change-requested'
            });
            break;

          // Cancel pending email change
          case 'cancel-pending-change':
            _.extend(valuesToSet, {
              emailChangeCandidate: '',
              emailRecoverable: '',
              emailProofToken: '',
              emailProofTokenExpiresAt: 0,
              emailRecoveryToken: '',
              emailRecoveryTokenExpiresAt: 0,
              emailStatus: 'confirmed'
            });
            break;

          // Otherwise, do nothing re: email
        }
      }

      if(inputs.hasOwnProperty('newPassword')) {
        await sails.helpers.passwords.checkPassword(inputs.password, this.req.me.password)
        .intercept('incorrect', 'badPassword');

        valuesToSet.password = await sails.helpers.passwords.hashPassword(inputs.newPassword);
      }

      // Save to the db
      await User.updateOne({id: this.req.me.id })
      .set(valuesToSet);

      // If an email address change was requested, and re-confirmation is required,
      // send the "confirm account" email.
      if (desiredEmailEffect === 'begin-change' || desiredEmailEffect === 'modify-pending-change') {
        await sails.helpers.sendTemplateEmail.with({
          to: newEmailAddress,
          subject: 'Confirm email change',
          template: 'email-verify-new-email',
          templateData: {
            username: inputs.username||this.req.me.username,
            token: valuesToSet.emailProofToken
          }
        });

        await sails.helpers.sendTemplateEmail.with({
          to: this.req.me.emailAddress,
          subject: 'This email is not used anymore',
          template: 'email-recover-email',
          templateData: {
            username: inputs.username||this.req.me.username,
            token: valuesToSet.emailRecoveryToken,
          }
        });
      }

      return exits.success();
    });
  }


};
