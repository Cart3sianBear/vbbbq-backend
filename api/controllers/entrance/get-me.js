module.exports = {


  friendlyName: 'GetMe',


  description: 'Send to client informations about the authenticated user',

  inputs: {

  },

  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'notAuthenticated',
      description: 'The request does not permit to find a User.',
    }

  },


  fn: async function (inputs, exits) {

    me = this.req.me;
    me.avatars = [];

    var meUser = await User.findOne({
      id: me.id
    }).populate('conversations').populate('invitations').populate('previousConversations');
    me.conversationsId = meUser.conversations.map( (element) => { return element.id } );
    me.invitationsId = meUser.invitations.map( (element) => { return element.id } );
    me.previousConversationsId = meUser.previousConversations.map( (element) => { return element.id } );

    exits.success(me.toJSON());

    // Store the socket ID
    await User.updateOne({ id:me.id }).set({ socketId:sails.sockets.getId(this.req) });

  }

};
