module.exports = {


  friendlyName: 'Signup',


  description: 'Sign up for a new user account.',


  extendedDescription:
`This creates a new user record in the database, signs in the requesting user agent
by modifying its [session](https://sailsjs.com/documentation/concepts/sessions), and
(if emailing with Mailgun is enabled) sends an account verification email.

If a verification email is sent, the new user's account is put in an "unconfirmed" state
until they confirm they are using a legitimate email address (by clicking the link in
the account verification message.)`,


  inputs: {

    emailAddress: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol00',
      description: 'The unencrypted password to use for the new account.'
    },

    username:  {
      required: true,
      type: 'string',
      example: 'MasterOfChaosDu51',
      description: 'The user\'s username.',
    },

    placeId: {
      required: true,
      type: 'number',
      description: 'The user\'s placeId in nominatim\s sense.',
    }

  },


  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },
  },


  fn: async function (inputs) {

    var newEmailAddress = inputs.emailAddress.toLowerCase();

    const osmQuery = new Promise( (resolve) => {
      sails.https.get(process.env.NOMINATIM_URL+"/details.php?place_id="+inputs.placeId+"&format=json&addressdetails=1", (res) => {
        var body = '';
        res.on('data', function (chunk) {
          body = body + chunk;
        });
        res.on('end',function(){
          switch(res.statusCode) {
            case 200:
              resolve(JSON.parse(body));
            break;  
          }
        });
      });
    });

    osmQuery.then( async (place) => {
      // Build up data for the new user record and save it to the database.
      // (Also use `fetch` to retrieve the new ID so that we can use it below.)
      var newUserRecord = await User.create(_.extend({
        emailAddress: newEmailAddress,
        password: await sails.helpers.passwords.hashPassword(inputs.password),
        username: inputs.username,
        placeId: place['place_id'],
        city: place.address.find( (element) => { return ['village','town','administrative','country'].includes(element.type); }).localname,
        latitude: place.centroid.coordinates['1'],
        longitude: place.centroid.coordinates['0'],
        // tosAcceptedByIp: this.req.ip
        dashboard: { wrappings: [], fullScreenConversation: null },
      }, sails.config.custom.verifyEmailAddresses? {
        emailProofToken: await sails.helpers.strings.random('url-friendly'),
        emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
        emailStatus: 'unconfirmed'
      }:{}))
      .intercept('E_UNIQUE', 'emailAlreadyInUse')
      .intercept({name: 'UsageError'}, 'invalid')
      .fetch();

      // Store the user's new id in their session.
      this.req.session.userId = newUserRecord.id;

      if (sails.config.custom.verifyEmailAddresses) {
        // Send "confirm account" email
        await sails.helpers.sendTemplateEmail.with({
          to: newEmailAddress,
          subject: 'Please confirm your account',
          template: 'email-verify-account',
          templateData: {
            username: newUserRecord.username,
            token: newUserRecord.emailProofToken
          }
        });
      } else {
        sails.log.info('Skipping new account email verification... (since `verifyEmailAddresses` is disabled)');
      }
    });
  }

};
