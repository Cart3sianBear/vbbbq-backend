module.exports = {


  friendlyName: 'Recover email',


  description:
`Recover the user's email address.`,


  inputs: {

    token: {
      description: 'The recovery token from the email.',
      example: '4-32fad81jdaf$329'
    }

  },


  exits: {

    success: {
      description: 'Email address confirmed and requesting user logged in.'
    },

    redirect: {
      description: 'Email address confirmed and requesting user logged in.  Since this looks like a browser, redirecting...',
      responseType: 'redirect'
    },

    invalidOrExpiredToken: {
      responseType: 'expired',
      description: 'The provided token is expired, invalid, or already used up.',
    },

    emailAddressNoLongerAvailable: {
      statusCode: 409,
      description: 'The email address is no longer available.',
      extendedDescription: 'This is an edge case that is not always anticipated by websites and APIs.  Since it is pretty rare, the 500 server error page is used as a simple catch-all.  If this becomes important in the future, this could easily be expanded into a custom error page or resolution flow.  But for context: this behavior of showing the 500 server error page mimics how popular apps like Slack behave under the same circumstances.',
    }

  },


  fn: async function (inputs) {

    // If no token was provided, this is automatically invalid.
    if (!inputs.token) {
      throw 'invalidOrExpiredToken';
    }

    // Get the user with the matching email token.
    var user = await User.findOne({ emailRecoveryToken: inputs.token });

    // If no such user exists, or their token is expired, bail.
    if (!user || user.emailRecoveryTokenExpiresAt <= Date.now()) {
      throw 'invalidOrExpiredToken';
    }

    if (!user.emailRecoverable){
      throw new Error(`Consistency violation: Could not update Stripe customer because this user record's emailRecoverable ("${user.emailRecoverable}") is missing.  (This should never happen.)`);
    }

    if (await User.count({ emailAddress: user.emailRecoverable }) > 0 && user.emailAddress != user.emailRecoverable) {
      throw 'emailAddressNoLongerAvailable';
    }

    // Finally update the user in the database, store their id in the session
    // (just in case they aren't logged in already), then redirect them to
    // their "my account" page so they can see their updated email address.
    await User.updateOne({ id: user.id })
    .set({
      emailStatus: 'confirmed',
      emailRecoveryToken: '',
      emailRecoveryTokenExpiresAt: 0,
      emailProofToken: '',
      emailProofTokenExpiresAt: 0,
      emailAddress: user.emailRecoverable,
      lastEmailChangeAt: Date.now(),
      emailChangeCandidate: '',
      emailRecoverable: '',
    });
    this.req.session.userId = user.id;
    if (this.req.wantsJSON) {
      return;
    } else {
      throw { redirect: '/account' };
    }

  }


};
