module.exports = async function testSocket(req, res) {
  if(req.body.text && req.body.text!='') {
    var users = await User.find({
      where: { username : { 'contains' : req.body.text }, id : { '!=' : req.me.id } },
    }).limit(5);
    users.forEach( (user) => {
      delete user.dashboard;
    });
  }
  else {
    var users = [];
  }

  var promise = Promise.resolve();

  users.forEach( (user) => {
    promise = promise.then( () => {
      return new Promise( async (resolve) => {
        sails.vincenty.distVincenty(req.me.latitude, req.me.longitude, user.latitude, user.longitude, function (distance) {
          user.distance = Math.round(distance/1000);
          resolve();
        });
      })
    });
  });

  promise = promise.then( () => {
    return res.json({
      users: users,
    });
  });

};
