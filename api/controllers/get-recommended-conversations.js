module.exports = {


  friendlyName: 'GetRecommendedConversations',


  description: 'Fill the meeting place',


  extendedDescription:
`This search for relevent conversations records in the database and send them to client`,


  inputs: {

  },


  exits: {

    success: {
      description: 'The requested recommended conversations were send successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

  },


  fn: async function (inputs, exits) {

    var recommendationsRaw = await sails.sendNativeQuery(`
      SELECT conversation.id, criteria(`+this.req.me.id+`,conversation.id), geographicDeviation(conversation.id) FROM conversation
      LEFT JOIN guest_invitation ON guest_invitation.invitation=conversation.id AND guest_invitation.guest=`+this.req.me.id+`
      LEFT JOIN participant_conversation ON participant_conversation.conversation=conversation.id AND participant_conversation.participant=`+this.req.me.id+`
      LEFT JOIN formerMember_previousConversation ON formerMember_previousConversation.previousConversation=conversation.id AND formerMember_previousConversation.formerMember=`+this.req.me.id+`
      WHERE conversation.status='public' AND guest_invitation.guest IS NULL AND participant_conversation.participant IS NULL AND formerMember_previousConversation.formerMember IS NULL
      ORDER BY criteria(`+this.req.me.id+`,conversation.id) DESC LIMIT 50;
    `);

    conversations = await Conversation.find({
      where: { id : { 'in' : recommendationsRaw.rows.map( (element) => { return element.id; } ) } }
    }).populate('participants').populate('guests').populate('formerMembers');

    exits.success({ conversations: conversations.map( (element) => { return element.toJSON(); }), recommendations: recommendationsRaw.rows.map( (element) => { return { id : element.id, criteria : element['criteria('+this.req.me.id+',conversation.id)'], geographicDeviation : element['geographicDeviation(conversation.id)'] } ; }) });

  }

};
