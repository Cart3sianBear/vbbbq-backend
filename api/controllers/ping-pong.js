module.exports = function testSocket(req, res) {
  if (!req.isSocket) {
    return res.badRequest();
  }

  if (req.me) {
    sails.log.info('Following logged in user made a ping-pong : \n'+req.me);
  }

  return res.json({
    message: 'pong'
  });
};
