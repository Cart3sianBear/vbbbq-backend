module.exports = {


  friendlyName: 'GetUsers',


  description: 'Send to client informations about users',


  extendedDescription:
`This search for users records in the database and send them to client`,


  inputs: {

    ids: {
      required: true,
      type: 'ref',
      description: 'The array of id associated with the requested users.',
    },

  },


  exits: {

    success: {
      description: 'The conversations users were send successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

  },


  fn: async function (inputs, exits) {
    var users = [];

    var promise = Promise.resolve();
    inputs.ids.forEach( (id) => {
      promise = promise.then( () => {
        return new Promise( async (resolve) => {
          var user = await User.findOne({ id: id });
          if(typeof user != 'undefined') {
            delete user.dashboard;
            users.push(user);
          }
          resolve();
        })
      });
    });

    promise = promise.then( () => {
      return new Promise( async (resolve) => {
        users.forEach( (user) => {
          promise = promise.then( () => {
            return new Promise( async (resolve2) => {
              sails.vincenty.distVincenty(this.req.me.latitude, this.req.me.longitude, user.latitude, user.longitude, function (distance) {
                user.distance = Math.round(distance/1000);
                resolve2();
              });
            })
          });
        });

        promise = promise.then( () => {
          exits.success(users.map( (element) => { return element.toJSON(); } ));
        });

        resolve();
      });
    });

  }

};
