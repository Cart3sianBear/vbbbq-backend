module.exports = {


  friendlyName: 'UpdateEntry',


  description: 'Update an entry',


  extendedDescription:
`This updates an entry`,


  inputs: {

    id: {
      required: true,
      type: 'number',
      description: 'The id of the entry.',
    },

    content: {
      required: true,
      type: 'string',
      description: 'The content of the edited message.',
    },

  },


  exits: {

    success: {
      description: 'The entry has been successfully updated.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    var entry = await Entry.findOne({
      id: inputs.id
    }).populate('user').populate('conversation');

    var conversation = await Conversation.findOne({
      id: entry.conversation.id
    }).populate('participants');

    if(!conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id) || entry.user.id!=this.req.me.id || entry.type!='message' || entry.deleted) {
      throw 'unauthorized';
    }

    entry = await Entry.updateOne({id: entry.id })
    .set({ data: { content: inputs.content }});

    sails.sockets.broadcast(['participants_'+conversation.id],'entryEdited', entry.toJSON(), this.req);

    exits.success(entry.toJSON());

  }

};
