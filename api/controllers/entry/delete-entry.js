module.exports = {


  friendlyName: 'DeleteEntry',


  description: 'Delete an entry',


  extendedDescription:
`This updates an entry, make blank its data field and set its deleted flag to true`,


  inputs: {

    id: {
      required: true,
      type: 'number',
      description: 'The id of the entry.',
    },

  },


  exits: {

    success: {
      description: 'The entry has been successfully updated.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    entry = await Entry.findOne({
      id: inputs.id
    }).populate('user').populate('conversation');

    if(entry.user.id!=this.req.me.id || entry.type!='message') {
      throw 'unauthorized';
    }

    entry = await Entry.updateOne({id: entry.id })
    .set({ deleted: true });

    sails.sockets.broadcast(['participants_'+entry.conversation],'entryEdited', entry.toJSON(), this.req);

    exits.success(entry.toJSON());

  }

};
