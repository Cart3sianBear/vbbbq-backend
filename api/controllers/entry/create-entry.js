module.exports = {


  friendlyName: 'CreateEntry',


  description: 'Create a new entry',


  extendedDescription:
`This creates a new entry record in the database`,


  inputs: {

    type: {
      required: true,
      type: 'string',
      description: 'The type of entry, in [message].',
    },

    conversation: {
      required: true,
      type: 'number',
      description: 'The id of the conversation to which belongs the entry',
    },

    content: {
      type: 'string',
      description: 'In case of a message entry, the content of the message',
    },

  },


  exits: {

    success: {
      description: 'New entry was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided inputs are invalid.',
    },

    unauthorized: {
      responseType: 'unauthorized',
      description: 'The request is forbidden for its author.',
    },

  },


  fn: async function (inputs, exits) {

    var conversation = await Conversation.findOne({
      id: inputs.conversation
    }).populate('participants');

    if(!conversation.participants.map( (element) => { return element.id; }).includes(this.req.me.id)) {
      throw 'unauthorized';
    }

    switch(inputs.type) {
      case 'message':
        var entry = await Entry.create({
          type: inputs.type,
          user: this.req.me.id,
          conversation: conversation.id,
          data: { content: inputs.content }
        }).fetch();
      break;
    }

    // var entry = await Entry.findOne({
    //   id: newEntryRecord.id
    // }).populate('user').populate('conversation');

    sails.sockets.broadcast(['participants_'+entry.conversation], 'entryCreated', entry.toJSON(), this.req);

    exits.success(entry.toJSON());

  }

};
