module.exports = {


  friendlyName: 'GetEntries',


  description: 'Send to client a limited number of entries associated with a conversation',


  extendedDescription:
`This search for entries records in the database and send them to client`,


  inputs: {

    conversation: {
      required: true,
      type: 'number',
      description: 'The id associated with the conversation.',
    },

    until: {
      type: 'number',
      description: 'The up limit of the request (for scroll loading).',
    },

  },


  exits: {

    success: {
      description: 'The requested entries were send successfully.'
    },

  },


  fn: async function (inputs, exits) {

    var conversation = await Conversation.findOne({ id: inputs.conversation }).populate('entries',{
      where: {
        user: this.req.me.id,
        type: {'in': ['join','leave']}
      },
    });

    var query = { or: [], };
    conversation.entries.forEach( (entry) => {
      if(entry.type=='join' && (query.or.length==0 || Object.keys(query.or[query.or.length-1].id).includes('<='))) {
        query.or.push({ id: { '>=': entry.id } });
      }
      else if(entry.type=='leave' && query.or.length!=0 && !Object.keys(query.or[query.or.length-1].id).includes('<=')) {
        query.or[query.or.length-1].id['<='] = entry.id;
      }
    });
    query.or.push({ type: 'create' });
    query.or.push({ type: 'invited', user: this.req.me.id });
    query.or.push({ type: 'leave', user: this.req.me.id });
    
    if(typeof inputs.until !== 'undefined') {
      query.id = { '<=': inputs.until };
    }

    conversation = await Conversation.findOne({ id: conversation.id }).populate('entries',{
      where: query,
      limit: process.env.GET_ENTRIES_LIMIT, // Strange bug here, sometimes it returns an empty array...
      sort: 'id DESC'
    });

    exits.success(conversation.entries.map( (element) => { return element.toJSON(); } ));

  }

};
