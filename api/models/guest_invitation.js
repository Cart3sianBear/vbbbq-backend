module.exports = {
  attributes: {

    guest: {
      model: 'user',
    },

    invitation: {
      model: 'conversation',
    },

    lastCheckedAt: {
      type: 'number',
    },

    lastClickedAt: {
      type: 'number',
    },

    lastReadAt: {
      type: 'number',
    },

  },

};
