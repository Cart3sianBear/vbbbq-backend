module.exports = {
  attributes: {
    emailAddress: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
      maxLength: 200,
      example: 'mary.sue@example.com'
    },

    emailStatus: {
      type: 'string',
      isIn: ['unconfirmed', 'change-requested', 'confirmed'],
      defaultsTo: 'confirmed',
      description: 'The confirmation status of the user\'s email address.',
      extendedDescription:
`Users might be created as "unconfirmed" (e.g. normal signup) or as "confirmed" (e.g. hard-coded
admin users).  When the email verification feature is enabled, new users created via the
signup form have \`emailStatus: 'unconfirmed'\` until they click the link in the confirmation email.
Similarly, when an existing user changes their email address, they switch to the "change-requested"
email status until they click the link in the confirmation email.`
    },

    emailChangeCandidate: {
      type: 'string',
      isEmail: true,
      description: 'A still-unconfirmed email address that this user wants to change to (if relevant).'
    },

    emailRecoverable: {
      type: 'string',
      isEmail: true,
      description: 'A recoverable email address.'
    },

    username: { type: 'string', required: true, unique: true, minLength: 3 },
    placeId: { type: 'number', required: true },
    city: { type: 'string', required: true },
    latitude: { type: 'number', required: true },
    longitude: { type: 'number', required: true },
    password: { type: 'string', required: true, protect: true, custom: function(value) {
      return _.isString(value) && value.length >= 8 && value.match(/[a-z]/i) && value.match(/[0-9]/);
    }},
    admin: { type: 'boolean', defaultsTo: false },

    passwordResetToken: {
      type: 'string',
      description: 'A unique token used to verify the user\'s identity when recovering a password.  Expires after 1 use, or after a set amount of time has elapsed.'
    },

    passwordResetTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `passwordResetToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },

    emailProofToken: {
      type: 'string',
      description: 'A pseudorandom, probabilistically-unique token for use in our account verification emails.'
    },

    emailProofTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `emailProofToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },

    emailRecoveryToken: {
      type: 'string',
      description: 'A pseudorandom, probabilistically-unique token for use in our email recovery emails.'
    },

    emailRecoveryTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `emailRecoveryToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },

    deleteAccountToken: {
      type: 'string',
      description: 'A pseudorandom, probabilistically-unique token for use in our account deletion emails.'
    },

    deleteAccountTokenExpiresAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment when this user\'s `deleteAccountToken` will expire (or 0 if the user currently has no such token).',
      example: 1502844074211
    },

    tosAcceptedByIp: {
      type: 'string',
      description: 'The IP (ipv4) address of the request that accepted the terms of service.',
      extendedDescription: 'Useful for certain types of businesses and regulatory requirements (KYC, etc.)',
      moreInfoUrl: 'https://en.wikipedia.org/wiki/Know_your_customer'
    },

    lastEmailChangeAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment at which this user most recently changed his email',
      example: 1502844074211
    },

    lastSeenAt: {
      type: 'number',
      description: 'A JS timestamp (epoch ms) representing the moment at which this user most recently interacted with the backend while logged in (or 0 if they have not interacted with the backend at all yet).',
      example: 1502844074211
    },

    socketId: {
      type: 'string',
      description: 'The ID of the socket related to this user',
      example: 'ksrw5U-fS1DiUp0JAAAA'
    },

    conversations: {
      collection: 'conversation',
      via: 'participant',
      through: 'participant_conversation'
    },

    invitations: {
      collection: 'conversation',
      via: 'guest',
      through: 'guest_invitation'
    },

    previousConversations: {
      collection: 'conversation',
      via: 'formerMember',
      through: 'formerMember_previousConversation'
    },

    entries: {
      collection: 'entry',
      via: 'user'
    },

    dashboard: {
      type: 'json',
      description: 'The current state of the user\'s dashboard',
      required: true,
    },
  },

  /*
  beforeCreate: function (valuesToSet, next) {
    sails.helpers.passwords.hashPassword(valuesToSet.password).exec((err, hashedPassword)=>{
      if (err) { return next(err); }
      valuesToSet.password = hashedPassword;
      return next();
    });
  },
  */

  beforeDestroy: async function (criteria, next) {
    var userRecord = await User.findOne(criteria.where).populate('conversations').populate('invitations').populate('previousConversations').populate('entries');

    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/tmp/'+userRecord.id+"'");
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/32/'+userRecord.id+"'");
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/40/'+userRecord.id+"'");
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/64/'+userRecord.id+"'");
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/128/'+userRecord.id+"'");
    sails.shell.exec("rm '"+process.env.AVATARS_PATH+'/original/'+userRecord.id+"'");

    await Entry.update({id: { 'in': userRecord.entries.filter( (entry) => { return entry.type=='message'; }).map( (entry) => { return entry.id; }) } })
    .set({ deleted: true });

    await User.removeFromCollection(userRecord.id, 'conversations', userRecord.conversations.map( (entry) => { return entry.id; }));
    await User.removeFromCollection(userRecord.id, 'invitations', userRecord.invitations.map( (entry) => { return entry.id; }));
    await User.removeFromCollection(userRecord.id, 'previousConversations', userRecord.previousConversations.map( (entry) => { return entry.id; }));

    userRecord.conversations.forEach( async (conversation) => {
      var conversationRecord = await Conversation.findOne({ id: conversation.id }).populate('participants').populate('guests');
      if(conversationRecord.participants.length==0) {
        await Conversation.removeFromCollection(conversationRecord.id, 'guests', conversationRecord.guests.map( (guest) => { return guest.id; }));
        await Conversation.updateOne({id: conversationRecord.id })
        .set({ status: 'archived' });

        sails.sockets.broadcast('guests_'+conversationRecord.id,'conversationDestroyed', conversationRecord.id);
        sails.sockets.removeRoomMembersFromRooms('guests_'+conversationRecord.id,'guests_'+conversationRecord.id);
      }
    });

    sails.sockets.leaveAll(userRecord.socketId);

    return next();
  },

  customToJSON: function() {
    return _.merge(_.omit(this, ['password', 'emailAddress', 'placeId', 'longitude', 'latitude', 'createdAt', 'updatedAt', 'deleteAccountToken', 'deleteAccountTokenExpiresAt', 'emailChangeCandidate', 'emailRecoverable', 'emailProofToken', 'emailProofTokenExpiresAt', 'emailRecoveryToken', 'emailRecoveryTokenExpiresAt', 'emailStatus', 'passwordResetToken', 'passwordResetTokenExpiresAt', 'lastEmailChangeAt', 'socketId', 'tosAcceptedByIp']),{ avatarsBuffered: { 32 : null, 40 : null, 64: null, 128: null, original : null, tmp : null } });
  }
};
