module.exports = {
  attributes: {

    status: {
      type: 'string',
      isIn: ['public','private','archived'],
      required: true,
    },

    participants: {
      collection: 'user',
      via: 'conversation',
      through: 'participant_conversation'
    },

    guests: {
      collection: 'user',
      via: 'invitation',
      through: 'guest_invitation'
    },

    formerMembers: {
      collection: 'user',
      via: 'previousConversation',
      through: 'formerMember_previousConversation'
    },

    entries: {
      collection: 'entry',
      via: 'conversation'
    },
  },

  customToJSON: function() {
    var JSON = this;

    if(typeof this.participants != 'undefined') {
      JSON = _.merge(JSON,{ participantsId: this.participants.map( (element) => { return element.id } ) });
    }
    if(typeof this.guests != 'undefined') {
      JSON = _.merge(JSON,{ guestsId: this.guests.map( (element) => { return element.id } ) });
    }
    if(typeof this.formerMembers != 'undefined') {
      JSON = _.merge(JSON,{ formerMembersId: this.formerMembers.map( (element) => { return element.id } ) });
    }
    JSON = _.merge(_.omit(JSON, ['createdAt', 'updatedAt','participants','guests','formerMembers','entries']), { entriesId: typeof this.entries != 'undefined' ? this.entries.map( (element) => { return element.id } ) : [] });
    return JSON;
  }
};
