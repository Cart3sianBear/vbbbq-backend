module.exports = {
  attributes: {

    type: {
      type: 'string',
      isIn: ['create','invited','join','leave','message'],
      required: true,
    },

    user: {
      model: 'user',
      required: true,
    },

    conversation: {
      model: 'conversation',
      required: true,
    },

    data: {
      type: 'json',
      required: true,
    },

    deleted: {
      type: 'boolean',
      defaultsTo: false,
    },
  },

  customToJSON: function() {
    var JSON = this;
    if(this.deleted) {
      JSON = _.merge(_.omit(JSON, ['data']), { data: { } });
    }
    return _.merge(_.omit(JSON, ['user', 'conversation']),{ userId: this.user.id==undefined ? this.user : this.user.id, conversationId: this.conversation.id==undefined ? this.conversation : this.conversation.id });
  }
};
