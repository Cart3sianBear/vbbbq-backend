module.exports = {
  attributes: {

    participant: {
      model: 'user',
    },

    conversation: {
      model: 'conversation',
    },

    lastCheckedAt: {
      type: 'number',
    },

    lastClickedAt: {
      type: 'number',
    },

    lastReadAt: {
      type: 'number',
    },

  },

};
