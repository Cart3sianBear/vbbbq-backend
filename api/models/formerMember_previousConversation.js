module.exports = {
  attributes: {

    formerMember: {
      model: 'user',
    },

    previousConversation: {
      model: 'conversation',
    },

    lastCheckedAt: {
      type: 'number',
    },

    lastClickedAt: {
      type: 'number',
    },

    lastReadAt: {
      type: 'number',
    },

  },

};
