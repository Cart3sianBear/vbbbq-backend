/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': false,
  'security/grant-csrf-token': true,
  'ping-pong': true,

  'entrance/signup': true,
  'entrance/confirm-email': true,
  'entrance/recover-email': true,
  'entrance/login': true,
  'entrance/send-password-recovery-email': true,
  'entrance/update-password-and-login': true,
  'entrance/get-me': 'is-logged-in',
  'account/update-profile': 'is-logged-in',
  'account/update-avatar': 'is-logged-in',
  'account/crop-avatar': 'is-logged-in',
  'account/logout': 'is-logged-in',
  'account/delete-account': true,
  'account/confirm-deletion': true,

  'search': 'is-logged-in',
  'get-avatar': 'is-logged-in',

  'user/get-users': 'is-logged-in',
  
  'conversation/create-conversation': 'is-logged-in',
  'conversation/get-conversations': 'is-logged-in',
  'conversation/update-conversation': 'is-logged-in',
  'conversation/invite-in-conversation': 'is-logged-in',
  'conversation/join-conversation': 'is-logged-in',
  'conversation/leave-conversation': 'is-logged-in',

  'entry/create-entry': 'is-logged-in',
  'entry/get-entries': 'is-logged-in',
  'entry/update-entry': 'is-logged-in',
  'entry/delete-entry': 'is-logged-in',

  'get-recommended-conversations': 'is-logged-in',
};
