/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  sails.config.port = process.env.BACKEND_PORT;

  sails.https = require('https');
  sails.shell = require('shelljs');
  sails.sharp = require('sharp');
  sails.sharp.cache(false);
  sails.log.silly('sharp cache : '+JSON.stringify(sails.sharp.cache()));
  sails.vincenty = require('node-vincenty');


  await sails.sendNativeQuery(`
    DROP FUNCTION IF EXISTS Vincenty;
  `);
  await sails.sendNativeQuery(`
    CREATE FUNCTION Vincenty(
      lat1 FLOAT, lon1 FLOAT,
      lat2 FLOAT, lon2 FLOAT
    ) RETURNS FLOAT
    NO SQL
    DETERMINISTIC
    COMMENT 'Returns the distance in degrees on the Earth between two known points
      of latitude and longitude using the Vincenty formula from:
      http://en.wikipedia.org/wiki/Great-circle_distance'
    BEGIN
    RETURN DEGREES(
      ATAN2(
        SQRT(
          POW(COS(RADIANS(lat2))*SIN(RADIANS(lon2-lon1)),2) +
          POW(COS(RADIANS(lat1))*SIN(RADIANS(lat2)) -
            (SIN(RADIANS(lat1))*COS(RADIANS(lat2)) *
              COS(RADIANS(lon2-lon1))
            ),
            2
          )
        ),
        SIN(RADIANS(lat1))*SIN(RADIANS(lat2)) +
        COS(RADIANS(lat1))*COS(RADIANS(lat2))*COS(RADIANS(lon2-lon1))
      )
    );
    END
  `);

  await sails.sendNativeQuery(`
    DROP FUNCTION IF EXISTS weight;
  `);
  await sails.sendNativeQuery(`
    CREATE FUNCTION weight(
      lat1 FLOAT, lon1 FLOAT,
      lat2 FLOAT, lon2 FLOAT
    ) RETURNS FLOAT
    NO SQL
    DETERMINISTIC
    COMMENT 'Returns the individual weight of the geographic relation based on geographic distance'
    BEGIN
    DECLARE tau FLOAT DEFAULT `+process.env.RECOMMENDED_CONVERSATIONS_TAU+`;
    RETURN EXP(-Vincenty(lat1, lon1, lat2, lon2)/tau);
    END
  `);

  await sails.sendNativeQuery(`
    DROP FUNCTION IF EXISTS criteria;
  `);
  await sails.sendNativeQuery(`
    CREATE FUNCTION criteria(user INT, conversation INT) RETURNS FLOAT
    NO SQL
    DETERMINISTIC
    COMMENT 'Returns the sum of angular distances between the specified user and those which participate to the conversation'
    BEGIN
    DECLARE alpha FLOAT DEFAULT `+process.env.RECOMMENDED_CONVERSATIONS_ALPHA+`;
    DECLARE mylatitude, mylongitude FLOAT;
    SELECT latitude, longitude INTO mylatitude, mylongitude FROM user WHERE id=user;
    RETURN (SELECT SUM(weight(mylatitude, mylongitude, user.latitude, user.longitude))*(alpha*RAND()+1-alpha) FROM user 
      INNER JOIN participant_conversation ON participant_conversation.participant=user.id
      WHERE participant_conversation.conversation=conversation);
    END
  `);

  await sails.sendNativeQuery(`
    DROP FUNCTION IF EXISTS geographicDeviation;
  `);
  await sails.sendNativeQuery(`
    CREATE FUNCTION geographicDeviation(conversation INT) RETURNS FLOAT
    NO SQL
    DETERMINISTIC
    COMMENT 'Returns the (angular) 2D standard deviation of geographic positions of participants'
    BEGIN
    RETURN (SELECT SQRT(VARIANCE(user.latitude)+VARIANCE(user.longitude)) FROM user
      INNER JOIN participant_conversation ON participant_conversation.participant=user.id
      WHERE participant_conversation.conversation=conversation);
    END
  `);


  sails.io.on('disconnect', async (msg) => {
    await User.updateOne({ socketId:msg.id }).set({ socketId:null });
  });

};
