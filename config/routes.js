/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  '/security/grant-csrf-token':  { action: 'security/grant-csrf-token' },
  '/api/ping-pong': { action: 'ping-pong' },

  'POST /api/signup': { action: 'entrance/signup' },
  'POST /api/confirm-email': { action: 'entrance/confirm-email' },
  'POST /api/recover-email': { action: 'entrance/recover-email' },
  'PUT /api/login': { action: 'entrance/login' },
  'POST /api/forgotten-password': { action: 'entrance/send-password-recovery-email' },
  'POST /api/new-password': { action: 'entrance/update-password-and-login' },
  'GET /api/get-me': { action: 'entrance/get-me'},
  'POST /api/update-profile': { action: 'account/update-profile' },
  'POST /api/update-avatar': { action: 'account/update-avatar' },
  'POST /api/crop-avatar': { action: 'account/crop-avatar' },
  'GET /api/logout': { action: 'account/logout'},
  'POST /api/delete-account': { action: 'account/delete-account'},
  'POST /api/confirm-deletion': { action: 'account/confirm-deletion'},

  'POST /api/search': { action: 'search'},
  'GET /api/get-avatar?': { action: 'get-avatar'},

  'POST /api/get-users': { action: 'user/get-users'},

  'POST /api/create-conversation': { action: 'conversation/create-conversation'},
  'POST /api/get-conversations': { action: 'conversation/get-conversations'},
  'POST /api/update-conversation': { action: 'conversation/update-conversation'},
  'POST /api/invite-in-conversation': { action: 'conversation/invite-in-conversation'},
  'POST /api/join-conversation': { action: 'conversation/join-conversation'},
  'POST /api/leave-conversation': { action: 'conversation/leave-conversation'},

  'POST /api/create-entry': { action: 'entry/create-entry'},
  'POST /api/get-entries': { action: 'entry/get-entries'},
  'POST /api/update-entry': { action: 'entry/update-entry'},
  'POST /api/delete-entry': { action: 'entry/delete-entry'},

  'POST /api/get-recommended-conversations': { action: 'get-recommended-conversations'},


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
